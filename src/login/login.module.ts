import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from 'src/master/schemas/user.schema';
import { LoginController } from './login.controller';
import { LoginService } from './login.service';

@Module({
    imports: [MongooseModule.forFeature([
        {name:'Users', schema: UserSchema}
    ])
],
    controllers:[LoginController],
    providers: [LoginService],
})
export class LoginModule {}
