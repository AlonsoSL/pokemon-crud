
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from 'src/master/interfaces/user.interface';
import { LoginDTO } from './dto/login.dto';

@Injectable()
export class LoginService {

  constructor( @InjectModel('Users') private readonly userModel: Model<User>){}

  async login(loginDTO: LoginDTO):Promise<User> {
    const user =loginDTO.userName;
    const password =loginDTO.password;
    const  result = await this.userModel.findOne({userName: user});
    if(!result) throw new NotFoundException('credenciales incorrectas')
    if(result.status == "INACTIVE") throw new NotFoundException('credenciales incorrectas')
    if(result.password !== password) throw new NotFoundException('credenciales incorrectas')

    return result;
  }


 
}
