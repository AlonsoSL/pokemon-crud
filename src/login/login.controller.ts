import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Res } from "@nestjs/common";
import { LoginDTO } from './dto/login.dto';
import { LoginService } from "./login.service";


@Controller('/login')
export class LoginController {
    constructor( private readonly loginService: LoginService) {}


  @Post('')
  async createLearner(@Res() res, @Body() createLearnerDto: LoginDTO) {
     const user = await this.loginService.login(createLearnerDto);
    return res.status(HttpStatus.CREATED).json({
      message: 'login ok',
      user
    })
  }


    
}