import { HttpService } from '@nestjs/axios';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDTO } from './dto/create-user.dto';
import { User } from './interfaces/user.interface';
import { UpdateLearnerDTO } from './dto/update-learner.dto';
import { UnsubcribePokemonDTO } from './dto/unsubcribe-pokemon.dto';
import { CreateHistoryDTO } from './dto/create-history.dto';
import { Rol } from '../../dist/rol/interfaces/rol.interface';
import { MasterService } from './master.service';

@Injectable()
export class LearnerService {

  constructor(private readonly masterService: MasterService,
    @InjectModel('Users') private readonly learnerModel: Model<User>,
    @InjectModel('History') private readonly historyModel: Model<History>
    ){}



  async getLearnerById(learnerId: string):Promise<User>{
    const result = await this.learnerModel.findById(learnerId);
    return result;
  }

  async updateLearner(learnerId:String, updateLearnerDTO: UpdateLearnerDTO):Promise<User>{
    const updated =await this.learnerModel.findByIdAndUpdate(learnerId, updateLearnerDTO, {new: true});
    
    const dto = new CreateHistoryDTO();
    dto.modifyBy='test';
    dto.action ='UPDATE';
    dto.description='update learner by id';
    dto.updateRecordId = learnerId;
    await this.masterService.createHistory(dto);

    return updated;
  }

  async unsubcribePokemon(unsubcribePokemonDTO: UnsubcribePokemonDTO):Promise<User>{
    const nombre = unsubcribePokemonDTO.pokemonName;
    const id = unsubcribePokemonDTO.learnerId;
    const filter = { _id: id}

    console.log('nombre de pokemon ',nombre );
    const updated =await this.learnerModel.findOneAndUpdate(
        filter,
        { $pull : { pokemon: { pokemonName: nombre } } }, {new: true}
    );
    
    const dto = new CreateHistoryDTO();
    dto.modifyBy='test';
    dto.action ='UPDATE';
    dto.description='unsubscribe a pokemon to user';
    dto.updateRecordId = id;
    await this.masterService.createHistory(dto);

    return updated;
  }

 
}
