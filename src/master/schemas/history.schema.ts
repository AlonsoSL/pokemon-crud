import{ Schema } from 'mongoose';

export const HistorySchema = new Schema({
    modifyBy:String,
    action: String,
    description:String,
    updateRecordId: String,
    createdDate: {
        type: Date,
        default: Date.now
    },

});