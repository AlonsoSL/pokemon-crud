import{ Schema } from 'mongoose';

const PokemonSchema = new Schema({
    "pokemonName": {type:String},
    "pokemonStatus": {type:String,
        default: 'ACTIVE'}
  },{_id:false})

  
export const UserSchema = new Schema({
    userName: {
        type:String, required:true
    },
    rol: {
        type:String, required:true
    },
    password: String,
    pokemon : [PokemonSchema],

    createdDate: {
        type: Date,
        default: Date.now
    },
    status: {
        type: String,
        default: 'ACTIVE'
    }

});