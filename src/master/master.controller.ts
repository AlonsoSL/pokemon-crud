import { HttpService } from "@nestjs/axios";
import { Body, Controller, Delete, Get, HttpStatus, NotFoundException, Param, Post, Put, Res } from "@nestjs/common";
import { CreateUserDTO } from "./dto/create-user.dto";
import { UnsubcribePokemonDTO } from "./dto/unsubcribe-pokemon.dto";
import { UpdateLearnerDTO } from "./dto/update-learner.dto";
import { MasterService } from "./master.service";

@Controller('/master')
export class MasterController {
  constructor( private readonly httpService: HttpService,private readonly masterService: MasterService) {}

  @Post('/newLearner')
  async createLearner(@Res() res, @Body() createLearnerDto: CreateUserDTO) {
    const learner = await this.masterService.createLearner(createLearnerDto);
    return res.status(HttpStatus.CREATED).json({
      message: 'user Created',
      learner
    })
  }



  @Get('/findLearner/:learnerId')
  async getLerner(@Res() res, @Param('learnerId') learnerId) {
    const result= await this.masterService.getLearnerById(learnerId);
    if(!result) throw new NotFoundException('learner not found')
    return res.status(HttpStatus.OK).json({
      result
    })
  }


  @Put('/updateLearner/:learnerId')
 async updateLearner(@Res() res,@Param('learnerId') learnerId, @Body() updateLearnerDTO: UpdateLearnerDTO) {
    const learner = await this.masterService.updateLearner(learnerId,updateLearnerDTO);
    
    if(!learner) throw new NotFoundException('learner not found')
    return res.status(HttpStatus.CREATED).json({
      message: 'Learner Updated',
      rol: learner
    })
  }

  @Put('/unsubcribePokemon')
  async unsubcribePokemon(@Res() res, @Body() unsubcribePokemonDTO: UnsubcribePokemonDTO) {
     const pokemon = await this.masterService.unsubcribePokemon(unsubcribePokemonDTO);
     
     if(!pokemon) throw new NotFoundException('pokemon not found')
     return res.status(HttpStatus.CREATED).json({
       message: 'Learner Updated',
       rol: pokemon
     })
   }

   @Put('/changeMasterRol/:masterId')
   async changeRol(@Res() res, @Param('masterId')masterId) {
      const master = await this.masterService.changeMasterRol(masterId);
      
      if(!master) throw new NotFoundException('master not found')
      return res.status(HttpStatus.CREATED).json({
        message: 'Master Rol Updated',
        rol: master
      })
    }


@Get('/pokemons')
async bar(): Promise<any> {
    const response = await this.httpService.get('https://pokeapi.co/api/v2/pokemon').toPromise();

    return response.data;
}
}