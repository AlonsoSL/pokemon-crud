import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HitoryController } from './history.controller';
import { MasterController } from './master.controller';
import { MasterService } from './master.service';
import { HistorySchema } from './schemas/history.schema';
import { UserSchema } from './schemas/user.schema';
import { LearnerController } from './learner.controller';
import { LearnerService } from './learner.service';

@Module({
    imports: [MongooseModule.forFeature([
        {name:'Users', schema: UserSchema},
        {name:'History', schema: HistorySchema}
    ]),
    HttpModule.register({
        timeout: 5000,
        maxRedirects: 5,
      }),
],
    controllers:[MasterController, HitoryController, LearnerController],
    providers: [MasterService, LearnerService],
})
export class MasterModule {}
