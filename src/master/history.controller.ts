import { HttpService } from "@nestjs/axios";
import { Body, Controller, Delete, Get, HttpStatus, NotFoundException, Param, Post, Put, Query, Res } from "@nestjs/common";
import { CreateUserDTO } from "./dto/create-user.dto";
import { UnsubcribePokemonDTO } from "./dto/unsubcribe-pokemon.dto";
import { UpdateLearnerDTO } from "./dto/update-learner.dto";
import { MasterService } from "./master.service";

@Controller('/history')
export class HitoryController {
  constructor( private readonly masterService: MasterService) {}

  @Get('/checkRecordHistory')
  async checkRecordHistory(@Res() res, @Query('recordId') recordId, @Query('page') page: number, @Query('pageSize') pageSize: number) {
    const result= await this.masterService.getHistoryRecord(recordId,page,pageSize);

    if(!result) throw new NotFoundException('record not found')
    return res.status(HttpStatus.OK).json({
      result
    })
  }

  @Get('/getAll')
  async getHitory(@Res() res, @Query('page') page: number, @Query('pageSize') pageSize: number) {
    const result= await this.masterService.getHistory(page,pageSize);

    if(!result) throw new NotFoundException('record not found')
    return res.status(HttpStatus.OK).json({
      result
    })
  }

}