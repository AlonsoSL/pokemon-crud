import { HttpService } from "@nestjs/axios";
import { Body, Controller, Delete, Get, HttpStatus, NotFoundException, Param, Post, Put, Res } from "@nestjs/common";
import { CreateUserDTO } from "./dto/create-user.dto";
import { UnsubcribePokemonDTO } from "./dto/unsubcribe-pokemon.dto";
import { UpdateLearnerDTO } from "./dto/update-learner.dto";
import { LearnerService } from "./learner.service";
import { MasterService } from "./master.service";

@Controller('/learner')
export class LearnerController {
  constructor( private readonly learnerService: LearnerService) {}


  @Get('/findLearner/:learnerId')
  async getLerner(@Res() res, @Param('learnerId') learnerId) {
    const result= await this.learnerService.getLearnerById(learnerId);
    if(!result) throw new NotFoundException('learner not found')
    return res.status(HttpStatus.OK).json({
      result
    })
  }


  @Put('/updateLearner/:learnerId')
 async updateLearner(@Res() res,@Param('learnerId') learnerId, @Body() updateLearnerDTO: UpdateLearnerDTO) {
    const learner = await this.learnerService.updateLearner(learnerId,updateLearnerDTO);
    
    if(!learner) throw new NotFoundException('learner not found')
    return res.status(HttpStatus.CREATED).json({
      message: 'Learner Updated',
      rol: learner
    })
  }

  @Put('/unsubcribePokemon')
  async unsubcribePokemon(@Res() res, @Body() unsubcribePokemonDTO: UnsubcribePokemonDTO) {
     const pokemon = await this.learnerService.unsubcribePokemon(unsubcribePokemonDTO);
     
     if(!pokemon) throw new NotFoundException('pokemon not found')
     return res.status(HttpStatus.CREATED).json({
       message: 'Learner Updated',
       rol: pokemon
     })
   }

 
}