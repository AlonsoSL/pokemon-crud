import {Document} from 'mongoose'

export interface Hitory extends Document{
    readonly modifyBy: String;
    readonly action: String;
    readonly description: Date;

}