import {Document} from 'mongoose'

export interface User extends Document{
    readonly learnerName: String;
    readonly password: String;
    readonly createdDate: Date;
    readonly status: String;

}