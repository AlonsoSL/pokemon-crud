import { HttpService } from '@nestjs/axios';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDTO } from './dto/create-user.dto';
import { User } from './interfaces/user.interface';
import { UpdateLearnerDTO } from './dto/update-learner.dto';
import { UnsubcribePokemonDTO } from './dto/unsubcribe-pokemon.dto';
import { CreateHistoryDTO } from './dto/create-history.dto';
import { Rol } from '../../dist/rol/interfaces/rol.interface';

@Injectable()
export class MasterService {

  constructor(private readonly httpService: HttpService,
    @InjectModel('Users') private readonly learnerModel: Model<User>,
    @InjectModel('History') private readonly historyModel: Model<History>
    ){}

  async createLearner(createLearnerDto: CreateUserDTO):Promise<User> {
    const password = await this.generatePassword();
    createLearnerDto.password = password
    console.log('contrase;a creada ', password);
    const  result = await new this.learnerModel(createLearnerDto).save();
    
    const dto = new CreateHistoryDTO();
    dto.modifyBy='test';
    dto.action ='CREATE';
    dto.description='create new learner';
    dto.updateRecordId = result.id;
    await this.createHistory(dto);

    return result;
  }

  async getLearnerById(learnerId: string):Promise<User>{
    const result = await this.learnerModel.findById(learnerId);
    return result;
  }

  async updateLearner(learnerId:String, updateLearnerDTO: UpdateLearnerDTO):Promise<User>{
    const updated =await this.learnerModel.findByIdAndUpdate(learnerId, updateLearnerDTO, {new: true});
    
    const dto = new CreateHistoryDTO();
    dto.modifyBy='test';
    dto.action ='UPDATE';
    dto.description='update learner by id';
    dto.updateRecordId = learnerId;
    await this.createHistory(dto);

    return updated;
  }

  async unsubcribePokemon(unsubcribePokemonDTO: UnsubcribePokemonDTO):Promise<User>{
    const nombre = unsubcribePokemonDTO.pokemonName;
    const id = unsubcribePokemonDTO.learnerId;
    const filter = { _id: id}

    console.log('nombre de pokemon ',nombre );
    const updated =await this.learnerModel.findOneAndUpdate(
        filter,
        { $pull : { pokemon: { pokemonName: nombre } } }, {new: true}
    );
    
    const dto = new CreateHistoryDTO();
    dto.modifyBy='test';
    dto.action ='UPDATE';
    dto.description='unsubscribe a pokemon to user';
    dto.updateRecordId = id;
    await this.createHistory(dto);

    return updated;
  }

  async changeMasterRol(masterId:String):Promise<User>{    
    const updated =await this.learnerModel.findByIdAndUpdate({ _id: masterId}, { $set: {rol: 'LEARNER'} }, {new: true});
    
    const dto = new CreateHistoryDTO();
    dto.modifyBy='test';
    dto.action ='UPDATE';
    dto.description='change master rol by id';
    dto.updateRecordId = masterId;
    await this.createHistory(dto);

    return updated;
  }
  generatePassword():String{
    const minus = "abcdefghijklmnñopqrstuvwxyz";
    const mayus = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    
    let contraseña = "";
    for (let i = 0; i < 8; i++) {
      let eleccion = Math.floor(Math.random() * 3 + 1);
      if (eleccion == 1) {
        let caracter1 = minus.charAt(Math.floor(Math.random() * minus.length));
        contraseña += caracter1;
      } else if (eleccion == 2) {
        const caracter2 = mayus.charAt(Math.floor(Math.random() * mayus.length));
        contraseña += caracter2;
      } else {
        let num = Math.floor(Math.random() * 10);
        contraseña += num;
      }
    }
    return contraseña;
  }

//   async getPokemons():Promise<any[]>{
//     const pokems = (await this.httpService.get('https://pokeapi.co/api/v2/pokemon').toPromise()).data;
//     return pokems;
//   }

async getHistoryRecord(recordId: string, page:number, pageSize:number):Promise<History[]>{
  const result = await this.historyModel.find({updateRecordId:recordId}).skip(page).limit(pageSize);
  return result;
}

async getHistory( page:number, pageSize:number):Promise<History[]>{
  const result = await this.historyModel.find().skip(page).limit(pageSize);
  return result;
}

async createHistory(createHistoryDTO: CreateHistoryDTO):Promise<History> {
  const  result = await new this.historyModel(createHistoryDTO).save();
  return result;
}
}
