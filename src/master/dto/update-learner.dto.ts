import { Pokemon } from "./create-user.dto";

export class UpdateLearnerDTO{
    learnerName: String;
    password: String;
    pokemon: Pokemon;
   }