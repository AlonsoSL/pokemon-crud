export class CreateUserDTO{
    userName: String;
    rol: String;
    password: String;
    pokemon: Pokemon;

   }

   export interface Pokemon {
    pokemonName: string;
}