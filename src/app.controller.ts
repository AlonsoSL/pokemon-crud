import { Controller, Get } from '@nestjs/common';
import { AppService } from 'src/app.service';


@Controller('/pockemon')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/satus')
  getHello(): string {
    return this.appService.getStatus();
  }
}
