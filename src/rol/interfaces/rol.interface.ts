import {Document} from 'mongoose'

export interface Rol extends Document{
    readonly rolName: String;
    readonly createdDate: Date;
    readonly status: String;

}