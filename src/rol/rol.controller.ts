import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Res } from "@nestjs/common";
import { NotFoundException } from "@nestjs/common/exceptions";
import { AppService } from "src/app.service";
import { RolDTO } from './dto/Rol.dto';
import { RolService } from "./rol.service";

@Controller('/rol')
export class RolController {
  constructor(private readonly rolService: RolService) {}

    @Get()
  async getRols(@Res() res) {
    const result= await this.rolService.getRols();
    return res.status(HttpStatus.OK).json({
      rols: result
    })
  }

  @Get(':rolId')
  async getRol(@Res() res, @Param('rolId') rolId) {
    const result= await this.rolService.getRol(rolId);
    if(!result) throw new NotFoundException('no se encontro el rol')
    return res.status(HttpStatus.OK).json({
      rols: result
    })
  }


  @Post('/newRol')
  async createRol(@Res() res, @Body() rolDto: RolDTO) {
    const rol = await this.rolService.createRol(rolDto);
    return res.status(HttpStatus.CREATED).json({
      message: 'Rol Created',
      rol: rol
    })
  }



  @Put('/update/:rolId')
 async updateRol(@Res() res,@Param('rolId') rolId, @Body() rolDto: RolDTO) {
    const rol = await this.rolService.updateRol(rolId,rolDto);
    
    if(!rol) throw new NotFoundException('no se encontro el rol')
    return res.status(HttpStatus.CREATED).json({
      message: 'Rol Updated',
      rol: rol
    })
  }

  @Delete('delete/:rolId')
  async deleteRol(@Res() res, @Param('rolId') rolId) {
    const result= await this.rolService.deleteRol(rolId);
    if(!result) throw new NotFoundException('no se encontro el rol')
    return res.status(HttpStatus.OK).json({
      message: 'Rol deleted',
      rols: result
    })
  }
}