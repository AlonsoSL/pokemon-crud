import{ Schema } from 'mongoose';

export const RolSchema = new Schema({
    rolName: {
        type:String, required:true
    },
    createdDate: {
        type: Date,
        default: Date.now
    },
    status: {
        type: String,
        default: 'ACTIVE'
    }

});