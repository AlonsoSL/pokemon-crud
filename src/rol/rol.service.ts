import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Rol } from './interfaces/rol.interface';
import { RolDTO } from './dto/Rol.dto';

@Injectable()
export class RolService {

  constructor(@InjectModel('Rol') private readonly rolModel: Model<Rol>){}


  async getRols():Promise<Rol[]>{
    const rols = await this.rolModel.find();
    return rols;
  }

  async getRol(rolId: string):Promise<Rol>{
    const result = await this.rolModel.findById(rolId);
    return result;
  }

  async createRol(rolDTO: RolDTO):Promise<Rol> {
    const  result = await new this.rolModel(rolDTO).save();
    return result;
  }

  async updateRol(rolId:String, rolDTO: RolDTO):Promise<Rol>{
    const updated =await this.rolModel.findByIdAndUpdate(rolId, rolDTO, {new: true});
    return updated;
  }

  async deleteRol(rolId:String):Promise<Rol>{
    const deleted = await this.rolModel.findByIdAndDelete(rolId);
    return deleted;

  }
}
