import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { AppController } from './app.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { RolModule } from './rol/rol.module';
import { MasterModule } from './master/master.module';
import { LoginModule } from './login/login.module';

@Module({
  imports: [ MongooseModule.forRoot('mongodb+srv://consultor95:consultor95@miclusterpokemon.1znh6kd.mongodb.net/pokedev'), RolModule, MasterModule, LoginModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
